---
title: eerste blog
subtitle: periode 1
date: 2017-10-25
tags: ["blog", "blogging", "eerste blog"]
---


Hallo iedereen en welkom bij mijn eerste blog! dit is de eerste week waarbij ik uitgebreid ga vertellen over de dagen die ik heb op school. Ik zal vorige week kort vertellen en daarna zal ik vertellen over mijn aller eerste echte schoolweek aan de studie Communicatian and multi-media design.

> **Introductie week**
> We begonnen de week met een introductie kamp die 3 dagen duurden. Het begon op zondag 28 augustus in Capelle aan de IJsel bij scoutcentrum Rotterdam. we moesten hier om 4 uur zijn, zonder enig idee wat er zou gebeuren, we wisten alleen dat we gingen kamperen. Ik had geluk op het kamp en zag gelijk al een bekend gezicht, ik voelde me gelijk meer op mijn gemak want ik was niet alleen meer. de eerste dag was er nog niet veel gepland, we werden in teams opgedeeld, kregen we de teamnaam The Hobbit en speelde we een kennismakingsspel om elkaar beter te leren kennen. Hierna volgde het maken van shirts, avond eten en een spooktocht. De rest van de avond waren we vrij om te doen wat we wilden, dus gingen we bij het kampvuur zitten.
> De volgende ochtend werden we wakker gemaakt door het nummer "Wakker worden" van Jochem Meijer en gingen we ontbijten. de rest van de dag was ingevuld met de volgende challenges: de Marshmallow challenge, een meloenen race, vlot bouwen en een trailer maken voor een Sci-fi film, Sci-fi was het genre wat ons werd toe gedeeld. Door deze spellen werden we gelijk getest op onze creativiteit. Helaas had ik 2 van de 4 challenges, de Marshmallow challenge en een vlot bouwen,  al eerder gedaan dus wist ik al wat ik moest doen voor die opdrachten, maar de andere challenges waren zeer uitdagend voor mij. de rest van de avond moesten we groepjes maken van 5 en daar een foto van maken en naar een docent mailen. Mijn groepje bestond uiteindelijk uit 4 mensen, omdat we een 5de groepslid niet konden vinden,ondanks dat we zelfs hebben mee gedaan aan de speeddate. mijn groepje bestaat nu uit Aaron Plaizier, Ben Lim en Mark Magermans en de foto kunt u onder aan het kopje vinden.
> de volgende dag was de laatste dag dat we het kamp hadden, gelukkig werden we vandaag niet wakker gemaakt door een nummer van Jochem Meijer, maar door een stam die zei: ''*WIL DE GENE MET DE ROOIE SCOOTER BIJ HET HEK HEM VERPLAATSEN ER MOET NAMELIJK DAAR GEWERKT WORDEN!*'' Dit was ook niet heel fijn, maar beter dan dat nummer. Vandaag moesten we verschillende minigames doen, waaronder touwtrekken, hobbelfiets, broek hangen en nog veel meer, helaas was er iets fout gegaan met het wisselen, waardoor we niet alle min games gedaan hebben. Hierna werden de klassen ingedeeld en mochten we naar huis.
> de volgende dag begonnen we 09:20 met uitleg over de studie en hoe de dag in elkaar zat. we moesten met een paar klassen naar onze studio, waar we samen met ons team een voorwerp moesten maken dat voor ons team stond. Wij hadden de fakkel van de Olympische spelen nagemaakt met daar boven een infinity, wat onze team naam is geworden, ook hadden we een vlammen vlag gemaakt omdat we nog tijd over hadden. Hierna volgde een presentatie van alle voorwerpen die zijn gemaakt door de teams helaas kon ik hier niet bij zijn i.v.m mijn rijexamen, helaas ben ik gezakt. de volgende dag kregen we uitleg over ons aller eerste project van de studie en een hoorcollege die daarbij aansluit. Onze eerste project is een game ontwerpen voor de introductie week van Hogeschool Rotterdam, wat mij een erg leuk project lijkt. Mijn team en ik kregen gelijk al veel ideeën binnen over hoe we de game willen maken. In de middag hadden we een tour door Rotterdam, wat ik niet heel leuk vond om te doen aangezien ik de stad al kenden, want ik ben hier geboren en getogen. Dat was de laatste dag voor de week, want we kregen vrijdag vrij.
>![Team foto](https://lh3.googleusercontent.com/hhzkLewagB3bxH7F0xilylz16pnIBNxZcvKRY52sKV_OPgZNxF5aNiBSFMcluXDoNrUS15LQA7CB=s600 "teamfoto infinity.jpeg")

Vanaf hier ga ik beginnen met de eerste echte les week en zou ik ook wat meer over de dagen vertellen.

>**Maandag 4 September 2017**
>Vandaag zijn we, mijn projectgroep en ik, begonnen aan ons eerst project voor onze studie. we moeten voor dit project een game maken voor de introductie week van hogeschool Rotterdam. We begonnen eerst met brainstormen over wat we in de game wilden. We hadden niet veel moeiten met hoe het spel er uit ging zien, we zaten allemaal ongeveer al met het zelfde idee in ons hoofd. Mark en Aaron , twee van mijn teamleden, gingen toen gelijk al een paar designs maken voor de game, terwijl Ben en ik alvast een logo voor ons team gingen maken die Mark later nog mooi heeft gemaakt, zie foto. De dag ging snel voorbij ,omdat we erg druk bezig waren aan ons project.
> ![](https://0pqodw-ch3301.files.1drv.com/y4md0eEVYy1ULgPB84grUQV4AdKGEBEMTxB8n9E-jrOY5zhC39LDwWD_zW8K_Ob0_r1Q5pj9gFIu1WtZ-FLOKx1GqGKoxY208K7BOtr4iYmYHJmnHJBGxs8d6_ICvWXahB7LF3UOCav-7f1sFVWBLizCfgsRde1ISw-Ddvs_IBsMaof6VUhqm2SSOVxgxTdc-mKholPTtN2EltQwggKmklJwg?width=256&height=144&cropmode=none)

>**Dinsdag 5 september 2017**
>Vandaag had ik mijn eerste hoorcollege van de opleiding. het was een erg interessant hoorcollege over het ontwerpproces. Het hoorcollege werd gegeven door Raúl	Marínez-Orozco, die erg fijn kan presenteren. Hij hield goed de aandacht van de studenten er bij door een interactieve bezigheid er in te doen, namelijk antwoorden met groen en rood papiertje.

> **Woensdag 6 september 2017**
> Op deze dag hebben we gewerkt aan de rest van het spel. We hebben vandaag de spelregels gemaakt, de magic circle bepaald en de gebieden voor onze game gekenmerkt op een dag- en nachtkaart. voor de rest kregen we nog een cursus over hoe we moeten bloggen.

> **Donderdag 7 september 2017**
> vandaag hadden mijn klas en ik onze eerste werkcollege, bij een werkcollege ga je dieper in de stof die je hebt gekregen bij het hoorcollege en ga je aan de slag met die stof. We moesten een ontwerpproces maken en presenteren samen met je team voor de klas.

> **Vrijdag 8 september 2017**
> Vandaag had ik een vrije dag en die heb ik ook zo benut.

>**Maandag 11 september 2017**
> De eerste dag van de week staat weer in het teken van de design challenge. Vandaag hadden we vooral gewerkt aan onze moodboards en aan ons onderzoek naar onze doelgroep, CMD-studenten. Ook kregen we vandaag weer een workshop, dit maal ging het over het omgaan met Gitlab. Dat was het weer voor de maandag.
 
>**Dinsdag 12 september 2017**
> Er was weer een hoorcollege op de planning, dit maal over  verbeelden. De hoorcollege vond ik vandaag niet zo heel leuk, sterker nog erg saai. Het werd gepresenteerd door  Tim Fleumer en hij bleef elke keer net iets te lang hangen bij zijn voorbeelden, waardoor hij veel moeite kreeg met de aandacht van de groep er bij te houden en om het allemaal binnen de geplande tijd te blijven.

>**Woensdag 13 september 2017**
> Zoals elke woensdag was het vandaag weer tijd voor de design challange. vandaag hebben we vooral gewerkt aan onze eigen deliverables, zoals ons moodboard, ons onderzoek en het visualiseren van ons onderzoek, omdat die vrijdag af moesten zijn. ook hebben we gewerkt aan onze paperprototype en die zelfs af gekregen. Samen met mijn teamgenoot Ben hadden we vandaag de workshop prototyping, waar we kleine opdrachten over het maken van protoypes hebben gedaan. Ook hadden we vandaag onze eerste les met onze studiecoach, Gerhard Rekvelt. Hij zal ons dingen over de studie uitleggen en ons helpen als we vragen of problemen hebben.
 
> **Donderdag 14 september 2017**
> Vandaag begonnen we pas om 15:00 en hadden we een werkcollege gepland, waar we vooral opdrachten hebben gedaan die hielpen om het hoorcollege beter te begrijpen.
 
> **Vrijdag 15 september 2017** 
> Een les vrije dag die je normaal gesproken zou moeten benutten met school maar dat niet heb gedaan.

>**Maandag 18 september 2017**
> Vandaag hadden we een spel analyse, waarbij we onze paperprototype moesten presenteren. De feedback die we hierbij kregen was vooral positief, dus hadden wij een goed gevoel over het project. voor de rest van de dag zijn we vooral gaan werken aan onze presentatie voor woensdag.

>**Dinsdag 19 September 2017**
>Het was alweer tijd voor de 3de hoorcollege van het jaar. Dit maal ging het over prototyping en werd hij gegeven door Bob. Ik vond dat Bob het erg goed deed, hij hield de aandacht van de klas er goed bij en liet de klas goed meedoen. de rest van de dag waren we vrij.

>