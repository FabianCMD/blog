---
title: blog periode 1
subtitle: eerste blog!
date: 2017-10-24
tags: ["Blog", "blogging", "periode1"] 
---
Blog periode 1
==============

Hallo iedereen en welkom bij mijn eerste blog! dit is de eerste week waarbij ik uitgebreid ga vertellen over de dagen die ik heb op school. Ik zal vorige week kort vertellen en daarna zal ik vertellen over mijn aller eerste echte schoolweek aan de studie Communicatian and multi-media design.

**Maandag 4 September 2017**
Vandaag zijn we, mijn projectgroep en ik, begonnen aan ons eerst project voor onze studie. we moeten voor dit project een game maken voor de introductie week van hogeschool Rotterdam. We begonnen eerst met brainstormen over wat we in de game wilden. We hadden niet veel moeiten met hoe het spel er uit ging zien, we zaten allemaal ongeveer al met het zelfde idee in ons hoofd. Mark en Aaron , twee van mijn teamleden, gingen toen gelijk al een paar designs maken voor de game, terwijl Ben en ik alvast een logo voor ons team gingen maken die Mark later nog mooi heeft gemaakt, zie foto. De dag ging snel voorbij ,omdat we erg druk bezig waren aan ons project.
![](https://0pqodw-ch3301.files.1drv.com/y4md0eEVYy1ULgPB84grUQV4AdKGEBEMTxB8n9E-jrOY5zhC39LDwWD_zW8K_Ob0_r1Q5pj9gFIu1WtZ-FLOKx1GqGKoxY208K7BOtr4iYmYHJmnHJBGxs8d6_ICvWXahB7LF3UOCav-7f1sFVWBLizCfgsRde1ISw-Ddvs_IBsMaof6VUhqm2SSOVxgxTdc-mKholPTtN2EltQwggKmklJwg?width=256&height=144&cropmode=none)

**Dinsdag 5 september 2017**
Vandaag had ik mijn eerste hoorcollege van de opleiding. het was een erg interessant hoorcollege over het ontwerpproces. Het hoorcollege werd gegeven door Raúl	Marínez-Orozco, die erg fijn kan presenteren. Hij hield goed de aandacht van de studenten er bij door een interactieve bezigheid er in te doen, namelijk antwoorden met groen en rood papiertje.

 **Woensdag 6 september 2017**
 Op deze dag hebben we gewerkt aan de rest van het spel. We hebben vandaag de spelregels gemaakt, de magic circle bepaald en de gebieden voor onze game gekenmerkt op een dag- en nachtkaart. voor de rest kregen we nog een cursus over hoe we moeten bloggen.

 **Donderdag 7 september 2017**
Vandaag hadden mijn klas en ik onze eerste werkcollege, bij een werkcollege ga je dieper in de stof die je hebt gekregen bij het hoorcollege en ga je aan de slag met die stof. We moesten een ontwerpproces maken en presenteren samen met je team voor de klas.

**Vrijdag 8 september 2017**
Vandaag had ik een vrije dag en die heb ik ook zo benut.

**Maandag 11 september 2017**
De eerste dag van de week staat weer in het teken van de design challenge. Vandaag hadden we vooral gewerkt aan onze moodboards en aan ons onderzoek naar onze doelgroep, CMD-studenten. Ook kregen we vandaag weer een workshop, dit maal ging het over het omgaan met Gitlab. Dat was het weer voor de maandag.

**Dinsdag 12 september 2017**
Er was weer een hoorcollege op de planning, dit maal over  verbeelden. De hoorcollege vond ik vandaag niet zo heel leuk, sterker nog erg saai. Het werd gepresenteerd door  Tim Fleumer en hij bleef elke keer net iets te lang hangen bij zijn voorbeelden, waardoor hij veel moeite kreeg met de aandacht van de groep er bij te houden en om het allemaal binnen de geplande tijd te blijven.

**Woensdag 13 september 2017**
Zoals elke woensdag was het vandaag weer tijd voor de design challange. vandaag hebben we vooral gewerkt aan onze eigen deliverables, zoals ons moodboard, ons onderzoek en het visualiseren van ons onderzoek, omdat die vrijdag af moesten zijn. ook hebben we gewerkt aan onze paperprototype en die zelfs af gekregen. Samen met mijn teamgenoot Ben hadden we vandaag de workshop prototyping, waar we kleine opdrachten over het maken van protoypes hebben gedaan. Ook hadden we vandaag onze eerste les met onze studiecoach, Gerhard Rekvelt. Hij zal ons dingen over de studie uitleggen en ons helpen als we vragen of problemen hebben.
 
**Donderdag 14 september 2017**
Vandaag begonnen we pas om 15:00 en hadden we een werkcollege gepland, waar we vooral opdrachten hebben gedaan die hielpen om het hoorcollege beter te begrijpen.
 
**Vrijdag 15 september 2017** 
Een les vrije dag die je normaal gesproken zou moeten benutten met school maar dat niet heb gedaan.

**Maandag 18 september 2017**
Vandaag waren er presentaties over het concept waar ieder team de afgelopen weken aan heeft gewerkt, waarbij ik veel goeie ideeën heb gehoord van de andere teams. We kregen ook goeie en slechten feedback over het concept dat we hadden bedacht. Aan het einde van de dag kregen we helaas het nieuws dat we ons concept waar we de afgelopen weken hard aan hebben gewerkt in de prullenbak mogen gooien, want we moesten de methode "*kill your darling*" gebruiken. Dit werd door de meeste leerlingen niet erg fijn gevonden en begrepen sommige ook niet wat het nou was.

**Dinsdag 19 september 2017**
Het derde hoorcollege stond vandaag op de planning. Het werd gepresenteerd door Bob en hij deed het naar mijn mening erg goed, want hij begon met een goede aandachtstrekker, een vliegtuigje door de zaal gooien, en betrok het publiek er goed bij, door vragen te stellen aan ze.

**Woensdag 20 september 2017**
Na het nieuws van de kill your darling verwerkt te hebben dachten mijn team genoten en ik er al snel aan om niet meer door de stad te lopen en gewoon de stad te verkennen vanuit een lokaal, maar Ben vond dit niet een goed idee. Hierdoor ontstond er een discussie tussen Ben en de rest, deze werd uiteindelijk gewonnen door Ben, maar we wilden nu een deel rondlopen en een deel in een lokaal doen. De rest van de dag stond in het teken van studiecoaching.

**Donderdag 21 september 2017**
Vandaag waren we vrij, doordat we een studiedag hadden.

**Vrijdag 22 september 2017**
vandaag had ik samen met ben weer een workshop prototyping. we moesten een prototype maken over hoe we onze school gezonder maakten. Hoewel de meesten dachten aan iets met sporten of eten dachten wij gelijk aan iets wat veel wordt gedaan door studenten, maar wat eigenlijk heel erg ongezond is. Je raad het misschien al, het is natuurlijk roken! wij dachten aan een "Mosquito", een apparaat die hangjongeren weg jaagt door middel van een hoge piep die zij horen, voor de rokers, dus met een warmte sensor. Hierdoor hopen we dat de rokers weggaan voor de deur en zo misschien denken om het niet meer te gaan doen. Volgende les gaan we onze prototype testen. 

**Maandag 25 september 2017**
We gingen verder vandaag op het idee van deels in de stad rondlopen en deels in een lokaal werken. We kwamen als eerst uit om iets met straatnamen te doen, hierbij wilden we 1 deel de stad in laten lopen en de ander achter de computer met google maps, maar dat was eigenlijk niet een heel goed idee want je kon er makkelijk bij vals spelen. We hebben wel dit idee voorgelegd bij Bob, hij vond het idee met een deel binnen en een deel erg leuk en zij als advies dat we me misschien er voor moesten zorgen dat er niet geen "*cheaten*" was en dit doen door er voor te zorgen dat er bijvoorbeeld geen kaart is of dat je niet weet wat de kaart doet. Na een tijdje brainstormen naar zo'n concept kwamen we eindelijk uit op het idee dat er eerst een kaart en een route langs verschillende gebouwen gemaakt moest worden in Rotterdam binnen 15 minuten en dat ze dan weer binnen een uur deze route hebben kunnen volgen. Hierbij kon je elk vervoersmiddel gebruiken, dus OV, fiets, auto, enz.

**Dinsdag 26 september 2017**
Het was tijd voor de laatste hoorcollege van de periode. Hij ging dit maal over onderzoek. Ik ben helaas vergeten wat de naam was van de presentator, maar ik vond dat hij het niet heel goed deed. Ik weet alleen niet zeker of dit door hem komt of omdat ik het onderwerp "onderzoek" een saai onderwerp vind.

**Woensdag 27 september 2017**
We gingen vandaag verder met ons concept en hebben hem helemaal uitgewerkt, zodat we hem volgende week kunnen presenteren. Ook heb ik vandaag gewerkt aan de spelanalyses, want deze moeten binnenkort ingeleverd worden. voor de rest van de dag hadden we alleen nog een uurtje studiecoaching.

**Donderdag 28 september 2017**
Vandaag was er de laatste werkcollege van de periode. Het ging over onderzoek net zoals het hoorcollege van afgelopen dinsdag.

**Vrijdag 29 september 2017**
vandaag hadden we de 0-meting van tekenen, hoewel ik had geoefend op tekenen heb ik toch geen goed gevoel over. Ook had ik vandaag de laatste keer workshop prototyping, maar ik was helaas niet meer gegaan doordat de gene met wie ik samen had gewerkt er niet bij kon zijn vanwege dat zijn 0-meting tegelijk was met de workshop en hij had de prototype thuis liggen, dus vond ik dat het nutteloos zou zijn om er heen te gaan.

**Maandag 2 oktober 2017**
Vandaag hebben we ons spel getest en een spelanalyse gedaan van ons spel. We hebben aan een klasgenoot gevraagd of hij ons spel wilde spelen en of hij daarna zijn mening wilde geven. Hij vond het een leuk spel maar vond dat we iets langer de tijd moesten geven om een route te maken. Ik heb hiernaast ook nog mijn moodboard verbeterd en weer ingeleverd.

**Dinsdag 3 oktober 2017**
Omdat we alle hoorcolleges al gehad hebben zijn we vandaag vrij.

**Woensdag 4 oktober 2017**
Vandaag hadden we presentaties voor een alumni, een oud student van de opleiding, over ons concept. De presentatie ging erg goed en de feedback was vooral positief, ook waren er wel een paar pittige vragen maar die hebben we goed kunnen beantwoorden. Vandaag hadden we ook nog een studiecoach uurtje. Hij was dit keer wel belangrijk, want hij ging over het maken van STARRT, wat later in je leerdossier moet.

**Donderdag 5 oktober 2017**
Vandaag hadden we de 0-metingen van Engels en Nederlands. Ze gingen matig, dus niet goed maar ook niet slecht. Ook hadden we ons laatste werkcollege ,die ging over prototyping.

**Vrijdag 6 oktober 2017**
*Vrije dag*

**Maandag 9 oktober 2017**
Vandaag hebben we de feedback die we hebben gekregen over de presentaties verwerkt in een document en in ons project. En zijn we begonnen met het maken van een prototypen voor het concept zodat we hem kunnen presenteren op de expo.

**Dinsdag 10 oktober 2017**
*Vrije dag*

**Woensdag 11 oktober 2017**
we zijn verder gaan werken aan onze prototype en hadden een workshop over het maken van een STARRT. Ook hadden we nog studiecoaching.

**Donderdag 12 oktober 2017**
Het was een erg spannende dag, ik had namelijk mijn eerste tentamen op de opleiding. We mochten een a4-tje aan spiekbrief gebruiken en hij was meerkeuze, maar toch was ik bang dat het een erg moeilijke dag zou worden, maar door mij goed voor te berijden op de toets had ik na afloop een goed gevoel over de toets.

**Vrijdag 13 oktober 2017**
*Vrije dag*

>**Zaterdag 14 oktober 2017 t/m  zondag 22 oktober 2017**
>*Herfstvakantie*

**Maandag 23 oktober 2017**
Hoewel iedereen vandaag druk bezig was met het voorbereiden op de expo van woensdag waren mijn groepje en ik vooral bezig met het maken van ons leerdossier, dit komt doordat wij al alles af hadden voor de expo. Ook heb ik even de zelfde klasgenoot waarbij mijn team en ik ons spel getest hadden, zijn spel getest.

**Dinsdag 24 oktober 2017**
*Vrije dag*

**Woensdag 25 oktober 2017**
Vandaag hadden we de expo van ons concept. Hierbij konden we stickers verdienen en uitdelen als een soort publieksprijs. iedereen kreeg er drie om uit te delen. Wij hadden ons kraampje versierd met snoepgoed, zodat meerdere mensen naar ons wilde komen en zo ook ons concept konden aanhoren, waardoor ze ons hun sticker konden geven. Aan het eind van de expo hadden we 14 of 15 stickers verdient! Helaas was dit niet genoeg voor de publieksprijs, ook hadden we de prijs van de docenten niet gekregen. 

**Terugblikken op het eerste kwartaal**
Het eerste kwartaal verliep naar mijn mening goed. Ik vind het een leuke opleiding en ben gemotiveerd om meer te gaan leren in de aankomende kwartalen. Wat ik voor de volgende kwartalen wil veranderen is dat ik op de dagen dat ik vrij ben van school ook iets ga doen, zo ben ik nog niet zo goed in het werken met fotoshop en illustrator en zou ik deze dan graag zelf willen verbeteren in de vrije dagen van school.